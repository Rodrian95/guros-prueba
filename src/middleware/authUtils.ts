import jwt from 'jsonwebtoken'

export const createJWTToken = async () =>
  jwt.sign(
    {jwt: 'prueba'},
    `${process.env.JWT_SECRET}`, {
    algorithm: 'HS256',
    expiresIn: process.env.ACCESS_TOKEN_LIFE
  });

export const verifyJWTToken = async (token: string) =>
  jwt.verify(token, process.env.JWT_SECRET || '', {algorithms: ['HS256']})
