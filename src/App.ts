import ChallengeRouter from './routes/v0/challenge.routes';
import express from "express";
import bodyParser from "body-parser";
import helmet from "helmet";
import logger from "morgan";
import cors from "cors";

class App {
  public express: express.Application;

  constructor() {
    this.express = express();
    this.middleware();
    this.routes();
  }
  private middleware() {
    this.express.use(cors());
    this.express.use(logger("dev"));
    this.express.use(helmet());
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
  }

  private routes() {
    const router: express.Router = express.Router();
    router.get('/', (req: express.Request, res: express.Response) => {
      res.json({
        message: 'Hello World!',
      });
    });
    this.express.use('/', router);
    this.express.use('/guros', ChallengeRouter);
  }
}

export default new App().express;
