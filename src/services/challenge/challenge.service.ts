import pool from "../../database/index";
import { ADN } from "../../models/adn.interface";

class ChallengeService {
  constructor() {}

  async insertDna(adn: ADN) {
    return await pool.query("INSERT INTO adn (dna, mutation) VALUES($1, $2) RETURNING *",[JSON.stringify({dna: adn.dna}), adn.mutation]);
  }
  async getDna() {
    return await pool.query("SELECT * FROM adn");
  }
  async validateDna(dna: any) {
    return await pool.query("SELECT dna FROM adn WHERE dna = $1",[dna]);
  }
}
const challService = new ChallengeService();
export default challService;
